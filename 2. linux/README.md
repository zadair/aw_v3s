# linux

linux 内核我们使用全志官方 camdriod 里面的 linux，而不使用主线 linux，因为前者对摄像头的支持比较好，这里的支持指的是前者的代码里已经包含了摄像头开发的主要驱动代码，而后者则有所欠缺


## 构建

#### 获取源码

```
http://pan.baidu.com/s/1miQN1Ra
```

#### 配置和构建

```
cp ../../linux_bsp/config/lichee_BSP_config .config
make arch=ARM menuconfig
bash ./scripts/build_tiger-cdr.sh
```

## 细节