# rootfs

根文件系统有很多实现，这里我们用 buildroot，它可用于构建小而美的 linux 根文件系统，这个讲究够用就行

## 构建

#### 获取源码

```
wget https://buildroot.org/downloads/buildroot-2017.08.tar.gz
```

#### 配置和构建

```
make menuconfig
make
```

## 细节
