
# u-boot

顶层 RADEME 已经简要说明了 uboot 的主要作用，就是对芯片、板子外设做基本初始化，然后开始从 sd 卡拷贝必要内容到内存中，然后开始引导 linux 内核启动


## 构建

#### 获取源码
```
// 从 sd 卡启动
git clone https://github.com/Lichee-Pi/u-boot.git -b v3s-current
// 支持 spi flash
git clone https://github.com/Lichee-Pi/u-boot.git -b v3s-spi-experimental

```

#### 配置和构建

```
// 使用默认配置
make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- LicheePi_Zero_defconfig 

// 定制自己的配置，这里我们直接退出，还是使用默认配置
make ARCH=arm menuconfig

// 编译，大约几分钟完成
make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf-
```

编译完会生成 u-boot-sunxi-with-spl.bin，下面说一下它放到哪里

sd 卡中的系统镜像一般分为三个区（分区大小可以自己定义，足够放入对应的内容就可以）：

* boot区或者引导区 - 该部分没有文件系统，而是直接将二进制的bootloader(uboot)文件写入到 sd 卡 8k 的起始位置
* linux内核区 - fat文件系统，存放linux内核、引导参数文件 boot.csr、设备数文件（dtb）
* rootfs分区 - ext4文件系统，用来存放根文件系统和用户数据等


## 细节

关于 uboot 是如何进行初始化、拷贝、引导，实际上并不是 uboot 官方源码就直接支持任何一款板子，这些内容有一部分是需要用户根据自己的板子进行适配的

#### 引导

引导参数是 uboot 引导 linux 内核启动需要的参数，就是上面说的linux内核区需要的第二个文件 boot.csr，引导有两种方式实现

###### 方式一

```
在 include/configs/sun8i.h 中定义引导参数，直接编译到 uboot.bin 里面了，就不用放到 linux 内核分区里了

#define CONFIG_BOOTCOMMAND   "setenv bootm_boot_mode sec;" \
                             "setenv machid 1029:" \
                             "load mmc 0:1 0x41000000 uImage;" \
                             "load mmc 0:1 0x41d00000 script.bin;" \
                             "bootm 0x41000000;"


#define CONFIG_BOOTARGS      "setenv bootargs console=ttyS0,115200 panic=5 rootwait root=/dev/mmcblk0p2 earlyprintk rw"
```

###### 方式二

将引导参数生成 boot.scr，然后将文件放到 linux 内核分区中，建议采用此方式，改起来方便

```
将下面内容编辑到 boot.cmd 文件中， 执行：mkimage -C none -A arm -T script -d boot.cmd boot.scr，就生成了 boot.scr
不要尝试 vi 打开二进制文件，防止误修改

setenv bootargs console=ttyS0,115200 panic=5 rootwait root=/dev/mmcblk0p2 earlyprintk rw
setenv bootm_boot_mode sec
setenv machid 1029
load mmc 0:1 0x41000000 uImage
load mmc 0:1 0x41d00000 script.bin
bootm 0x41000000


1. setenv bootargs console=ttyS0,115200 panic=5 rootwait root=/dev/mmcblk0p2 earlyprintk rw: 设置了一个名为“bootargs”的环境变量，该变量包含Linux内核引导参数。其中，“console=ttyS0,115200”表示将控制台输出重定向到串行端口；“panic=5”表示当内核崩溃时等待5秒后自动重启；“rootwait”表示等待根文件系统就绪后再引导；“root=/dev/mmcblk0p2”表示根文件系统所在的设备和分区；“earlyprintk”表示在内核启动早期就打印调试信息；“rw”表示将根文件系统挂载为可读写模式。
2. setenv bootm_boot_mode sec: 设置了一个名为“bootm_boot_mode”的环境变量，指定了U-Boot使用安全启动模式来引导内核。
3. setenv machid 1029: 设置了一个名为“machid”的环境变量，指定了目标系统的机器ID。
4. load mmc 0:1 0x41000000 uImage: 从SD卡的第1个分区（mmc 0:1）加载Linux内核映像文件（uImage）到内存地址0x41000000。
5. load mmc 0:1 0x41d00000 script.bin: 从SD卡的第1个分区（mmc 0:1）加载二进制配置文件（script.bin）到内存地址0x41d00000。
6. bootm 0x41000000: 引导已加载到内存中的Linux内核映像文件，该文件的起始地址为0x41000000。

```

#### 设备树

arm linux 有标准的 dtb 文件，全志 arm 自己定义了一下，叫 fex，清楚这两个概念是一回事就可以了

