# aw_v3s

本项目力求以最简单的方式让初学者能把 v3s 用起来，把嵌入式平台当成普通 pc 一样作为开发平台，降低学习门槛

#### 介绍

基于全志v3s，面向视频图像处理、对象识别，本项目主要介绍图像部分，芯片其它方面暂略去

#### 芯片简介

1. 单核 ARM Cortex-A7 CPU，频率 1.2GHz，内置 64MB DDR2 RAM
2. 可从 NOR\NAND FLASH、SD卡、USB 启动，片上有 32k 的 ROM 空间作为启动跳板，本板卡有 16MB 的 nor flash 和 sd 卡插槽
3. 支持 H264 编解码

#### 开发环境

建议使用 win 的 wsl 搭建 linux 开发环境，优点是安装简单、方便 win-linux 相互传输文件，缺点是 win 上面的设备（比如sd卡和usb摄像头）在linux上无法识别，当然这些问题我们可以规避解决

1. 下载 [交叉编译器](https://releases.linaro.org/components/toolchain/binaries/latest-7/arm-linux-gnueabihf/gcc-linaro-7.5.0-2019.12-x86_64_arm-linux-gnueabihf.tar.xz)，解压到随意目录，将其中的 bin 目录加入到环境变量中
2. 安装依赖 apt-get install lib32ncurses5-dev lib32z1 device-tree-compiler make gcc build-essential libncurses5 libncurses5-dev python2 bc sunxi-tools
3. u-boot：git clone https://github.com/Lichee-Pi/u-boot.git -b v3s-current
4. bsp-linux：http://pan.baidu.com/s/1eRJrViy
5. rootfs：wget https://buildroot.org/downloads/buildroot-2017.08.tar.gz


#### 基础概念

要运行操作系统，需要构建 uboot、linux Kernel、rootfs，然后将构建的文件制作出镜像文件，并写入到内存卡（sd卡）制作出启动卡，先解释几个概念

1. uboot，相当于 pc 的 bios，复杂点的芯片都需要前置引导程序来启动，告诉主芯片板子上都有什么输入输出设备；51单片机的引导程序固化在芯片内、stm32有start.S、越复杂的芯片就越需要定制化的引导程序
2. 嵌入式芯片应该叫 soc，与通用平台比如 pc intel i7 最大的差别是 i7 就是个 CPU，而 soc 是包含如 ram rom 外设的片上系统，能单独启动
3. linux kernel 是内核固件，rootfs 是管理块设备的文件系统
4. 镜像文件，镜像就是一个存储器内容的完全复制，可能有人会问，ctrl+c 不就是复制文件吗，它跟镜像有什么区别？镜像是包括文件系统自身部分的所有内容完全复制，并不是单纯的文件复制，文件从A复制到B，内容虽然没变，但是存储块顺序大概率变了
5. 制作镜像相对比较麻烦，需要分区、格式化、挂载、写入，对初学者不太友好，我这里提供一个命令行工具，直接将必要内容写入镜像文件，就可以在 win 下制作 sd 启动卡
6. sd卡启动，在测试环节，sd卡启动方式比较方便，当然也可以写入 nor flash 中启动，二者不冲突
7. 初学者，建议只关注最重要的部分，也就是应用层的开发，先不要急于搞懂 uboot、linux kernel、rootfs 原理，只要把应用程序编译出来放到 rootfs 文件系统中进行自己的测试就行了
8. 我们的三大组件，还是参考[荔枝派 zero](https://en.wiki.sipeed.com/soft/Lichee/zh/Zero-Doc/Start/board_intro.html)的指导来构建，注意这些组件并非版本越新越好，特别是内核，更新挺频繁的，没必要跟进到最新
9. 因为用到 usb otg 和 dvp 两种摄像头，所以内核开启了对其的支持（在荔枝派文档里有说明）

#### 构建说明

遇到报错不要怕，仔细查看文档+搜索引擎，一般都可以解决问题；如果对 linux 开发环境不太熟悉，可以先看一下 [linux 后端开发入门](https://gitee.com/zadair/lcg)  

1. [uboot](https://gitee.com/zadair/aw_v3s/tree/master/1.%20u-boot/README.md)  
2. [linux](https://gitee.com/zadair/aw_v3s/tree/master/2.%20linux/README.md)  
3. [rootfs](https://gitee.com/zadair/aw_v3s/tree/master/1.%20rootfs/README.md)   

